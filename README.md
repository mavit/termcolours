# NAME

**termcolours** - Automatically set unique terminal colour schemes

# SYNOPSIS

termcolours \[**--osc**\] \[**-n**\] \[**-D**|**-L**\] \[_hostname_\]

termcolours **--xrdb** \[**-n**\] \[**-D**|**-L**\] _hostname_ ...;
alias xterm="xterm -name xterm-\`hostname\`"

# DESCRIPTION

Give terminals a persistent unique background colour, generated from the hostname (or some other string of your choosing).  Set the sixteen ANSI colours to contrast with the background, using shades from the Tango palette.

# OPTIONS

- _hostname_

    The terminal background colour will be set based on a hash of the string _hostname_, somewhere between 0x000000 (black) and 0x404040 (dark), or 0xc0c0c0 (light) and 0xffffff (white).  Foreground colours are selected to contrast with the background.

    Defaults to the current system hostname, if not specified.

- **--dry-run**, **-n**

    Don't actually change the terminal colour.

- **--is-dark**, **-D**

    Exit 1 if the terminal is set to having a light background.

- **--is-light**, **-L**

    Exit 1 if the terminal is set to having a dark background.

- **--osc**

    Set the colour scheme for the current terminal, by sending it control sequences.

    This is the default behaviour.

- **--xrdb**, **-x**

    Set the colour scheme for `xterm`s started in the future, via X11 resources of the form `XTerm-{argument}*vt100*foreground` and `XTerm-{argument}*vt100*background`.

    You must tell `xterm` which resource to look at, with something like:

        xterm -name "xterm-$HOSTNAME"

- **--verbose**, **-v**

    Be more chatty.

- **--help**, **-h**

    Displays brief help.

- **--man**

    Displays the full manual.

# EXAMPLES

## Set the prompt colour depending on the terminal colour

Add something like the following to your `~/.bash_profile`:

    if termcolours --is-dark; then
        prompt_colour='37;40'
    else
        prompt_colour='30;47'
    fi
    PS1="\[\033[0;${prompt_colour}m\]\h:\W\\$\[\033[0m\] "

For a list of potential colour codes you could use here, see the Wikipedia page [ANSI escape code](https://en.wikipedia.org/wiki/ANSI_escape_code#3-bit_and_4-bit).

## Set the background colour for a container, without having to install `termcolours` inside the container

Add the following to your `~/.bashrc`:

    function docker {
        __termcolours_then_container "$FUNCNAME" "$@"
    }
    function podman {
        __termcolours_then_container "$FUNCNAME" "$@"
    }
    function __termcolours_then_container {
        declare -a args=("$@")

        if [[ "${args[1]}" == 'exec' || "${args[1]}" == 'run' ]]; then
            local arg
            local rc

            shift 2
            for arg in "$@"; do
                if [[ "$arg" != -* ]]; then
                   termcolours "$arg"
                   break
                fi
            done
        fi

        command "${args[@]}"
        rc=$?

        if [[ "${args[1]}" == 'exec' || "${args[1]}" == 'run' ]]; then
            termcolours
        fi

        return $rc
    }

# COMPATIBILITY

The following terminal emulators are known to work.

## Using OSC escape sequences

### Full support

- [Ghostty](https://ghostty.org/)
- [GNOME Console](https://apps.gnome.org/Console/)
- [GNOME Terminal](https://gitlab.gnome.org/GNOME/gnome-terminal)
- [Guake](https://guake.github.io/)
- [kitty](https://sw.kovidgoyal.net/kitty/)
- [PuTTY](https://www.chiark.greenend.org.uk/~sgtatham/putty/) (provided that environment variable `TERM` is set to `putty`)
- [rxvt-unicode](http://software.schmorp.de/pkg/rxvt-unicode.html)
- [st](https://st.suckless.org/)
- [Termux](https://termux.dev/)
- [Xfce Terminal](https://docs.xfce.org/apps/terminal/start)
- [XTerm](https://invisible-island.net/xterm/) (except very old versions, of the kind you may get with a proprietary Unix)

### Partial support

- The [Linux console](https://en.wikipedia.org/wiki/Linux_console)

## Using X11 resources

- XTerm (any version)

# SEE ALSO

- [https://www.mavit.org.uk/termcolours/](https://www.mavit.org.uk/termcolours/)
- [xrdb(1)](http://man.he.net/man1/xrdb)
- [xterm(1)](http://man.he.net/man1/xterm)
- [X11 resources](https://www.x.org/releases/current/doc/man/man7/X.7.xhtml#heading14)

# AUTHOR

[Peter Oliver](mailto:terminalcolours@mavit.org.uk)

# LICENCE

Copyright 2008, 2017, 2024 Peter Oliver.

This program is part of **termcolours**.

**termcolours** is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

**termcolours** is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with termcolours. If not, see [https://www.gnu.org/licenses/](https://www.gnu.org/licenses/).
