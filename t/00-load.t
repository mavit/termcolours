#!perl

use v5.26;
use warnings;

use FindBin;
use Test::More;

plan tests => 1;

BEGIN {
    ok(
        do "$FindBin::Bin/../script/termcolours"
    ) || print "Bail out!\n";
}

diag( "Testing App::TerminalColours $App::TerminalColours::VERSION, Perl $], $^X" );
