#!perl

# Copyright 2008, 2024 Peter Oliver.
#
# This file is part of termcolours.
#
# termcolours is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# termcolours is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the GNU General Public License
# along with termcolours. If not, see <https://www.gnu.org/licenses/>.

use v5.26;
use Test2::V0;

use FindBin;

plan(4);
do "$FindBin::Bin/../script/termcolours" or die "$!";

my @foo = App::TerminalColours::colour_hash('foo');
ok($foo[0], 'Host "foo" has a dark background');
is($foo[1], [0x4b2e, 0x2635, 0x332f], 'Foo has expected colour');

my @bar = App::TerminalColours::colour_hash('bar');
ok(not($bar[0]), 'Host "bar" has a light background');
is($bar[1], [0xcded, 0xc746, 0xd29d], 'Bar has expected colour');
