#!perl

# Copyright 2008, 2024 Peter Oliver.
#
# This file is part of termcolours.
#
# termcolours is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# termcolours is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the GNU General Public License
# along with termcolours. If not, see <https://www.gnu.org/licenses/>.

use v5.26;
use warnings;

use FindBin;
use Test::More;
do "$FindBin::Bin/../script/termcolours" or die "$!";

if ( exists $ENV{'DISPLAY'} ) {
    plan tests => 2;
}
else {
    plan skip_all => '$DISPLAY not set; X11 unavailable';
}

ok(
    App::TerminalColours::override_xterm_resources('foo'),
    'Function override_xterm_resources returned success'
);

my $count = 0;
open my $xrdb, '-|', qw(xrdb -query)
    or die "Couldn't open xrdb: $!";
while ( defined(my $line = <$xrdb>) ) {
    ++$count if $line =~ m/^xterm-foo\*/;
}
close $xrdb or die "Error on closing xrdb: $!";
is($count, 27, 'Count of X resources for xterm-foo');
